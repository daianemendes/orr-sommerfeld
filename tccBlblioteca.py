#modulos python 
import numpy as np
import warnings
import sys
import pandas as pd

"""

Módulo para a integração das equações de Blasius e Orr-Sommerfeld 
através do método de Runge-Kutta de 4ª ordem.

"""

def rk4(funcao, varIndep, varDep, args = ()):
    
    """Método de Runge-Kutta de 4ª ordem para integrar a soluçao de Blasius. 

        Parameters
        ----------
        
        funcao: function
            Solução de Blasius. 
        
        varIndep: Array of float
            Vetor com os valores de eta.    
        
        varDep: Array of float
            Sistema de EDOs de primeira ordem para a solução de Blasius. 
        
        *args:
            Informações necessárias à função, como a esmativa 
            da CI desconhecida de Blasius.
    
        Returns
        -------
        Array of float
            Solução do sistema de EDOs de Blasius em cada 
            passo de integraçao.
    """
    numeroPassos = len(varIndep)
   
    for i in range(1, numeroPassos):   
        
        esp =  varIndep[i] - varIndep[i-1]   #passo
        
        #calculo do Runge-Kutta de 4ª ordem
        k1 = esp*funcao( varIndep[i-1], varDep[i-1] , *args )
        k2 = esp*funcao( varIndep[i-1] + esp*0.5  , varDep[i-1] + k1*0.5 , 
                        *args )
        k3 = esp*funcao( varIndep[i-1] + esp*0.5  , varDep[i-1] + k2*0.5 , 
                        *args )
        k4 = esp*funcao( varIndep[i-1] + esp      , varDep[i-1] + k3     ,
                        *args )
        
        #Resulltado da integraçao
        varDep[i] = varDep[i-1] + ( k1 + 2 * k2 + 2 * k3 + k4) / 6

    return varDep
    
#Runge-Kutta com Gram-Schmidt
def rk4gs(funcao, varIndep, varDep1, varDep2, args=()):
    
    """Método de Runge-Kutta de 4ª ordem com Gram-Schmidt para 
    o cálculo de soluções linearmente independentes da 
    equação de Orr-Sommerfeld.

        Parameters
        ----------
        
        funcao: function
            Função de Orr-Sommerfeld que será integrada. 
        
        varIndep: Array of float
            Valores de y desde ym até 0 (sobre a superfície da placa).
        
        varDep1: Array of complex64
            Vetor da solução não-viscosa. 
            
        varDep2: Array of complex64
            Vetor da solução viscosa. 
        
        *args:
            Informações necessárias à integraçao, como alfa, 
            Re e c.
        
        Returns
        -------
        Array of complex64, Array of complex64
            Retorna os vetores das soluções linearmente independentes.
    """
    warnings.simplefilter("error", RuntimeWarning)
   
    numeroPassos = len(varIndep)
    for i in range(1, numeroPassos):   
        
        esp =  varIndep[i] - varIndep[i-1]
        
        #matriz da soluçao não-viscosa
        k1 = esp*funcao( varIndep[i-1], varDep1[i-1] , *args )
        k2 = esp*funcao( varIndep[i-1] + esp*0.5, varDep1[i-1] + k1*0.5 ,
                        *args )
        k3 = esp*funcao( varIndep[i-1] + esp*0.5, varDep1[i-1] + k2*0.5 ,
                        *args )
        k4 = esp*funcao( varIndep[i-1] + esp, varDep1[i-1] + k3, *args )
        varDep1[i] = varDep1[i-1] + ( k1 + 2 * k2 + 2 * k3 + k4) / 6
        
        #matriz da soluçao viscosa
        try:
            k1a = esp*funcao( varIndep[i-1], varDep2[i-1] , *args )
            k2a = esp*funcao( varIndep[i-1] + esp*0.5, 
                             varDep2[i-1] + k1a*0.5 , *args )
            k3a = esp*funcao( varIndep[i-1] + esp*0.5, 
                             varDep2[i-1] + k2a*0.5 , *args )
            k4a = esp*funcao( varIndep[i-1] + esp, varDep2[i-1] 
                             + k3a, *args )
        
        #tratamento de exceção para que o código calcule o 
        #próximo passo de integraçao
        except:
            print("k1", k1a, esp,  varIndep[i-1], varDep2[i-1], *args)
            print("func k1", funcao( varIndep[i-1], varDep2[i-1] ,*args ))
            print("k2", k2a, varIndep[i-1] + esp*0.5, 
                  varDep2[i-1] + k1a*0.5, *args )
            print("func k2", funcao( varIndep[i-1] + esp*0.5, 
                                    varDep2[i-1] + k1a*0.5 , *args ))
            print("k3", k3a, esp, varIndep[i-1] + esp*0.5, 
                  varDep2[i-1] + k2a*0.5, *args)
            print("func k3", funcao( varIndep[i-1] + esp*0.5, 
                                    varDep2[i-1] + k2a*0.5 , *args ))
            print("k4", k4a, esp, varIndep[i-1] + esp , varDep2[i-1] + k3, 
                  *args)
            print("func k4", funcao( varIndep[i-1] + esp, 
                                    varDep2[i-1] + k3a, *args ))
            
            raise ValueError("except")
           
        varDep2[i] = varDep2[i-1] + ( k1a + 2 * k2a + 2 * k3a + k4a) / 6
        
        #calculo do Gram-Schmidt
        alfaGS = np.linalg.norm(varDep1[i, :])   #normaliza o vetor da soluçao não-viscosa
        varDep1[i, :] = varDep1[i, :] / alfaGS   #ortogonaliza a soluçao não-viscosa
        beta = np.dot(varDep2[i, :], np.conjugate( varDep1[i, :]) )  
        varDep2[i, :] = varDep2[i, :] - (beta * varDep1[i, :])  #ortogonaliza a soluçao viscosa
        
    return varDep1[i, :], varDep2[i, :]

   






