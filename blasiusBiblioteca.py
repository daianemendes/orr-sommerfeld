#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 24 08:15:24 2020

@author: daiane
"""

import numpy as np
import tccBiblioteca as tcc
from scipy import optimize 

"""
Módulo para o cálculo da soluçao de Blasius com base na referência Schlichting 1979.

"""

#soluçao de Blasius
def blasiusODE(varIndep, varDep):
    
    """Método para resolver a equaçao de Blasius através de um 
    sistema de EDOs. 

        Parameters
        ----------
        
        varIndep: Array of float
            Vetor com os valores de $\eta$.    
        
        varDep: Array of float
            Sistema de EDOs de primeira ordem para a solução de Blasius. 
    
        Returns
        -------
        Array of float
            Solução da equação de Blasius.
    """
    return np.array([varDep[1], varDep[2], 
                     - varDep[0]*varDep[2] ])     #sistema de eqs da EDO

def h(p, varIndep, varDep):
    
    """Método para encontrar o valor de f''(0) através de uma 
    estimativa inicial. 

        Parameters
        ----------
        
        p: float
            Estimativa para a condição inicial da Solução de Blasius. 
        
        varIndep: Array of float
            Vetor com os valores de $\eta$.    
        
        varDep: Array of float
            Sistema de EDOs de primeira ordem para a solução de Blasius. 
    
        Returns
        -------
        float
            Retorna o resultado de h de acordo com a estimativa dada.  
    """
    varDep[0] = np.array([0.0, 0.0, p])    #condiçao inicial
    resultadoBlasius = tcc.rk4(blasiusODE, 
                               varIndep, 
                               varDep ) #calcula Blasius sob CIs diferentes
    return resultadoBlasius[-1, 1] - 1   

def blasiusSolution(size, etaInf = 10.0):
    
    """Método para encontrar o valor de f''(0) a partir de uma estimativa inicial 
    através do método de Newton-Raphson e em seguida, 
    calcular a solução de Blasius por Runge-Kutta de 4ª ordem. 

        Parameters
        ----------
        
        size: int
            Comprimento de eta.  
        
        etaInf: int
            Valor de eta na borda da camada limite. 
            
        
        Returns
        -------
        Array of float, Array of float
            Retorna o perfil de velocidades de Blasius no intervalo 
            de eta escolhido.
    """
    numeroEquacoes = 3
    
    etaValor = np.linspace( 0, etaInf, size, dtype = np.float64) #vetor com valores de eta
    fEta = np.zeros(( size, numeroEquacoes))  #matriz para guardar os resultados da soluçao de Blasius 
    fPrime2_0 = optimize.newton( h , 0.3, args=(etaValor, fEta))   #método de Newton-Raphson
    fEta[0] = np.array([0.0, 0.0, fPrime2_0])  #condiçoes iniciais da soluçao de Blasius
    etaValor[0] = 0    #inicia a integraçao na superficie da placa plana
    tcc.rk4(blasiusODE, etaValor, fEta  )     #integra Blasius por Runge-Kutta 
    return etaValor,  fEta 


