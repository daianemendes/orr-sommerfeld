#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 01:01:43 2020

@author: daiane
"""

import numpy as np
import matplotlib.pyplot as plt
import tccBiblioteca as tcc
import blasiusBiblioteca as bl
import warnings
from scipy import optimize
from scipy.interpolate import interp1d
from scipy import linalg
import pandas as pd
from csv import writer
from matplotlib.pyplot import rc
rc('text', usetex = True)



#calculo da soluçao de blasius
blasiusEtaInf = 10   #comprimento de eta no infinito
blasiusSize = 1000   #numero de passos de integraçao da soluçao de Blasius
aDelta = 1.72   #constante para calcular a espessura de deslocamento

#resolve a soluçao de Blasius
etaValor, resolveBlasius = bl.blasiusSolution(blasiusSize, blasiusEtaInf)

#interpolar $f'(\eta)$ e $f'''(\eta)$ 
fprime1 = interp1d(etaValor, resolveBlasius[:, 1])  
fprime3 = interp1d(etaValor, -resolveBlasius[:, 0] * resolveBlasius[:, 2])  


#orr-sommerfeld

def c3(yBarra, c, alfa, Re):
    #Legenda: c = $\bar{c}$, alfa = $\bar{\alpha}$ e Re = $Re_{\delta_1}$
    # yBarra = $\bar{y}$
    
    """Calculo do coeficiente C_3

        Parameters
        ----------
        yBarra: int
            Posição normal à placa plana.
        c: np.complex64
            Velocidade da onda de perturbaçao.
        alfa: float64
            Número de onda da perturbaçao.
        Re: int
            Número de Reynolds do escoamento.

        Returns
        -------
        complex
        Valor do coeficiente variável C_3(y)
      """    
    eta = (aDelta * yBarra) / np.sqrt(2.0)   #calculo de $\eta$
    vBarra = fprime1(eta)    #calcula $U(\bar{y})$
    return -2 * alfa**2 + -1j * Re * alfa * (vBarra - c)


def d3(yBarra, c, alfa, Re):
     #Legenda: c = $\bar{c}, alfa = $\bar{\alpha} e Re = $Re_{\delta_1}$
    # yBarra = $\bar{y}$ 
    
    """Calculo do coeficiente D_3

        Parameters
        ----------
        yBarra: int
            Posição normal à placa plana.
        c: np.complex64
            Velocidade da onda de perturbaçao.
        alfa: float64
            Número de onda da perturbaçao.
        Re: int
            Número de Reynolds do escoamento.

        Returns
        -------
        complex
        Valor do coeficiente variável D_3(y).
    """    
    eta = (aDelta * yBarra) / np.sqrt(2.0) #calcula $\eta$ 
    vBarra = fprime1(eta)   #calculo de $U(\bar{y})$
    vBarraLinhas = fprime3(eta) * aDelta**2 * 0.5 #calculo de $U''(\bar{y})$
    return alfa**4 + 1j*Re * alfa**3 * (vBarra 
                                        - c) + 1j*Re * alfa * vBarraLinhas 


def orrSommerfeld(varIndep, varDep, c, alfa, Re): 
    
     #Legenda: c = $\bar{c}, alfa = $\bar{\alpha} e Re = $Re_{\delta_1}$
    # yBarra = $\bar{y}$ 
    
    """Calculo da equação de Orr-Sommerfeld a partir de um 
    sistema de EDOs de primeira ordem. 

        Parameters
        ----------
        varIndep: float
            Vetor com valores de y.
            
        varDep: int
            Posiçao normal à placa.
            
        c: complex64
            Número de onda da perturbação.
        
        alfa: float64
            Número de onda da perturbaçao.
        Re: int
            Número de Reynolds do escoamento.

        Returns
        -------
        Array of complex64
        Vetor com as autofunçoes da equaçao de Orr-Sommerfeld
    """    
    yBarra = varIndep    #integraçao em y
    c3Coef = c3(yBarra, c, alfa, Re)
    d3Coef = d3(yBarra, c, alfa, Re)
    return np.array([varDep[1], varDep[2], varDep[3], 
                     -d3Coef*varDep[0] -c3Coef*varDep[2]], 
                    dtype=np.complex64)  


def resolveOrrSommerfeld(c, alfa, Re, numeroPassosOs=300, ym=3.0): 
     #Legenda: c = $\bar{c}, alfa = $\bar{\alpha} e Re = $Re_{\delta_1}$
    # yBarra = $\bar{y}$, ym = $\bar{y_m}$
    
    """Calculo da equação de Orr-Sommerfeld considerando as soluções 
      viscosa e não-viscosa no ponto ym

        Parameters
        ----------
        c: complex64
            Número de onda da perturbação.
        
        alfa: float64
            Número de onda da perturbaçao.
        Re: int
            Número de Reynolds do escoamento.

        numeroPassosOs: int
            Número de passos de integração da equação de Orr-Sommerfeld.
        
        ym: float
            Ponto fora da camada limite por onde será iniciada a solução.
        
        Returns
        -------
        y: Array of float
        amplitude1: Array of complex64
        amplitude2: Array of complex64
        
        Retorna um vetor de y e dois outros vetores com as soluções viscosa
        e não-viscosa calculadas em cada valor de y.        
    """   
    numeroEquacoesOs = 4   
    y = np.linspace(ym, 0, numeroPassosOs, dtype=np.float64) #calcula $\bar{y}$
    
    #vetor para guardar as amplitudes
    amplitude1 = np.zeros((numeroPassosOs, 
                           numeroEquacoesOs), 
                          dtype=np.complex64)   #matriz de $\bar{\Phi}_{I}$
    amplitude2 = np.zeros((numeroPassosOs, 
                           numeroEquacoesOs), 
                          dtype=np.complex64)   #matriz de $\bar{\Phi}_{II}$
        
    #calculo de lambda da soluçao viscosa ($\bar{\lambda}$)
    lambdaVar = np.sqrt(alfa**2 + 1j * Re * alfa * ( 1 - c))

    #condiçoes iniciais da equaçao de Orr-Sommerfeld
    #amplitude1[0] = $\bar{\Phi}_{I}(\bar{(y_m)})$
    #amplitude2[0] = $\bar{\Phi}_{II}(\bar{(y_m)})$
    amplitude1[0] = 1, alfa * -1, alfa**2, alfa**3 * -1    #solucao não viscosa em ym (Phi_I)
    amplitude2[0] = 1, lambdaVar * -1, lambdaVar**2, lambdaVar**3 * -1   #soluçao viscosa  em ym (Phi_II)
    
    #integra a equacao de Orr-Sommerfeld
    resolveOS = tcc.rk4gs(orrSommerfeld, y, amplitude1, 
                          amplitude2, args=(c, alfa, Re))
    
    return y, amplitude1, amplitude2

def fOs(c, alfa, Re, numeroPassosOs=300, ym=3.0):
      #Legenda: c = $\bar{c}, alfa = $\bar{\alpha} e Re = $Re_{\delta_1}$
      # yBarra = $\bar{y}$, ym = $\bar{y_m}$, #numeroPassosOs = $h$
    
    """Calculo do determinante para obtenção das soluções 
      não-triviais da equação de Orr-Sommerfeld 

        Parameters
        ----------
        
        c: complex64
            Número de onda da perturbação.
        
        alfa: float64
            Número de onda da perturbaçao.
        Re: int
            Número de Reynolds do escoamento.
            
        numeroPassosOs: int
            Número de passos de integração da equação de Orr-Sommerfeld.
        
        ym: float
            Ponto fora da camada limite por onde será iniciada a solução.

        Returns
        -------
        complex64
            Retorna o valor do determinante da matriz de coeficientes da 
            equação de Orr-Sommerfeld.
    """
    y, amplitude1, amplitude2 = resolveOrrSommerfeld(c, alfa, Re, 
                                                     numeroPassosOs, ym)
    
    #matriz dos coeficientes da equaçao de Orr-Sommerfeld
    matrizAutoValor = np.array([amplitude1[-1, :], 
                                amplitude2[-1, :], 
                                [0, 0, -1, 0], [0, 0, 0, -1]])
    
    return np.linalg.det(matrizAutoValor)    
   
def secante(f, x0, x1, tol=1E-5, args=(), log = False, maxiter = 50):
    
    """Método da secante para o cálculo de c, alfa ou Re por meio 
      de duas estimativas. 

        Parameters
        ----------
        
        f: function
            Função que calcula o determinante da matriz de coeficientes da
            equação de Orr-Sommerfeld. 
        
        x0: complex64
            Primeira estimativa para a raiz da função.
        
        x1:complex64
            Segunda estimativa para a raiz da função. 
            
        numeroPassosOs: int
            Número de passos de integração da equação de Orr-Sommerfeld.
        
        tol: int
            Tolerância para o cálculo da raiz. 
            Para c, recomenda-se utilizar 10e-6.
            Para alfa, utilize 10e-4.
            Para Re, o ideal é 10e-2.
        
        *args:
            Informações necessárias à função fOs, como alfa, 
            Re, c, ym e numeroPassos.
        
        maxiter: int
            Número máximo de iterações que o método realiza. 
            Se ultrapassado, ocorre uma exception. Para resolver, aumentar maxiter.

        Returns
        -------
        complex64
            Retorna o valor da raiz da função fOs para o parâmetro desejado,
            por exemplo, encontra alfa para ci = 0,010. 
    """
    warnings.simplefilter("error", "RuntimeWarning")
    
    f0 = f(x0, *args)    #calculo da funçao para a estimativa x0
    f1 = f(x1, *args)    #calculo da funçao para a estimativa x1
    i = 1  
    
    while (abs(x0-x1) > tol):     
        i += 1

        try:    
            x2 = x1 - f1 / (f1 - f0) * (x1 - x0)  #calculo da raiz da funçao
        
        except:
            print("x0, x1, x2, f0, f1", x0, x1, x2, f0, f1)
            raise ValueError("divisao por zero")
       
        if (i == maxiter):
            raise ValueError("Chegou na maxima iteracao")

        if (log):             
            print("Secante", i, x0, x1, x2, f0, f1 )
                        
        f0 = f1
        x0 = x1
        x1 = x2
        f1 = f(x1, *args)    
    return x1   

#parametros para o cálculo das curvas de estabilidade
numeroPassosOs = 1000   #passos de integraçao
ym = 4.5   #ponto fora da camada limite

def cImaginario(alfa, Re, c, args=()):
     #Legenda: c = $\bar{c}, alfa = $\bar{\alpha} e Re = $Re_{\delta_1}$
      # yBarra = $\bar{y}$, ym = $\bar{y_m}$, #numeroPassosOs = $h$
       
    """Calculo da parte imaginária de c a partir dos parâmetros desejados 
      para o ramo superior da curva de estabilidade. 

        Parameters
        ----------
        
        alfa: float64
            Número de onda da perturbaçao.
        Re: int
            Número de Reynolds do escoamento.
        
        c: complex64
            Número de onda da perturbação.
            
        *args:
            Parâmetros necessários para o calculo de c imaginário, como
            número de passos de integração e ym. 

        Returns
        -------
        complex64
            Parte imaginária de c. 
    """    
    cEncontrado = secante(fOs, c[0], c[1], tol=1e-06, 
                          args=(alfa, Re, numeroPassosOs, ym), 
                          log = False, maxiter = 100)
    c[0] = c[1]   
    c[1] = cEncontrado
    return (np.imag(cEncontrado))

def cImaginario2(Re, alfa, c, args=()):
     #Legenda: c = $\bar{c}, alfa = $\bar{\alpha} e Re = $Re_{\delta_1}$
     # yBarra = $\bar{y}$, ym = $\bar{y_m}$, #numeroPassosOs = $h$
     
    """Calculo da parte imaginária de c a partir dos parâmetros desejados 
      para o ramo inferior da curva de estabilidade. 

        Parameters
        ----------
        Re: int
            Número de Reynolds do escoamento.
            
        alfa: float64
            Número de onda da perturbaçao.
        
        c: complex64
            Número de onda da perturbação.
            
        *args:
            Parâmetros necessários para o calculo de c imaginário, como
            número de passos de integração e ym. 

        Returns
        -------
        complex64
            Parte imaginária de c. 
     """
    cEncontrad = secante(fOs, c[0], c[1], tol=1e-06, 
                         args=(alfa, Re, numeroPassosOs, ym), 
                         log = False, maxiter = 100)
    c[0] = c[1]
    c[1] = cEncontrad
    return (np.imag(cEncontrad))

def cImag(alfa, Re, c, cte=0.0, args=()):
     #Legenda: c = $\bar{c}, alfa = $\bar{\alpha} e Re = $Re_{\delta_1}$
      # yBarra = $\bar{y}$, ym = $\bar{y_m}$, #numeroPassosOs = $h$
    
    """Calculo da parte imaginária de c a partir de uma cte para o 
    ramo superior da curva de estabilidade. 

        Parameters
        ----------
        alfa: float64
            Número de onda da perturbaçao.
        
        Re: int
            Número de Reynolds do escoamento.
        
        c: complex64
            Número de onda da perturbação.
            
        cte: float
            Valor para a parte imaginaria da onda de perturbação. 
            Se cte > 0,0, tem-se ondas instáveis. 
            Para cte < 0,0, as ondas serão estáveis. 
            E cte = 0,0 significa ondas neutras. 
            
        *args:
            Parâmetros necessários para o calculo de c imaginário, como
            número de passos de integração e ym. 

        Returns
        -------
        complex64
            Parte imaginária de c desejada. 
    """
    return cImaginario(alfa, Re, c, args=(numeroPassosOs, ym)) - cte

def cImag2(Re, alfa, c, cte=0.0, args=()):
     #Legenda: c = $\bar{c}, alfa = $\bar{\alpha} e Re = $Re_{\delta_1}$
      # yBarra = $\bar{y}$, ym = $\bar{y_m}$, #numeroPassosOs = $h$
    
    """Calculo da parte imaginária de c a partir de uma cte para o 
    ramo inferior da curva de estabilidade. 

        Parameters
        --------- 
        Re: int
            Número de Reynolds do escoamento.
            
        alfa: float64
            Número de onda da perturbaçao.
        
        c: complex64
            Número de onda da perturbação.
            
        cte: float
            Valor para a parte imaginaria da onda de perturbação. 
            Se cte > 0,0, tem-se ondas instáveis. 
            Para cte < 0,0, as ondas serão estáveis. 
            E cte = 0,0 significa ondas neutras. 
            
        *args:
            Parâmetros necessários para o calculo de c imaginário, como
            número de passos de integração e ym. 

        Returns
        -------
        complex64
            Parte imaginária de c desejada. 
    """    
    return cImaginario2(Re, alfa, c, args=(numeroPassosOs, ym)) - cte


def extrapola(x,y,X):
    
    """Extrapolaçao das estimativas iniciais dos parâmetros da equação de 
     Orr-Sommerfeld. Implementado para tornar a simulaçao mais rápida e 
     aproximar as estimativas de forma precisa.

        Parameters
        --------- 
        x: complex64
            Valor do domínio no ponto anterior.
            
        y: complex64
            Valor do contra-domínio no ponto anterior.
        
        X: complex64
            Valor atual no domínio em que se deseja obter o valor 
            no contra-domínio. 

        Returns
        -------
        complex64
            Valor atual da imagem de X.
    """
    if (len(x) > 1):
        return y[-1] + ((X - x[-2]) / (x[-1] - x[-2]))*(y[-1] - y[-2])
    else:
        return y[-1]


#construção da curva de estabilidade

ReG = 10000   #valor de $Re_{\delta_1}$ para iniciar a curva
a1 = 0.21     #estimativa para $\bar{\alpha}$
a2 = 0.20     #estimativa para $\bar{\alpha}$
c0 = 0.10     #estimativa para $\bar{c}$
c1 = 0.15     #estimativa para $\bar{c}$
passosIntegracao = 1000   #$h$
ymOs = 4.5   #valor de $\bar{y_m}$
incrementaRe = True

#passo entre os cálculos
dReMax = 500
dRe = -dReMax  #decrementa 500 no valor do próximo $Re_{\delta_1}$
dAlfa = -0.015   #decrementa 0,015 no valor do próximo $\bar{\alpha}$

#listas para guardar os resultados
ReVetor = []
alfaVetor = []
cVetor = []

#inicializa um vetor para $\bar{c}$
cG = np.zeros( 2 , dtype = np.complex64)   

#calcula dois valores de $\bar{c}$
cG[0] = secante(fOs, c0, c1, tol=1e-06, 
                args=(a1, ReG, passosIntegracao, ymOs), 
                log = True, maxiter = 100)
cG[1] = secante(fOs, c0, c1, tol=1e-06, 
                args=(a2, ReG, passosIntegracao, ymOs), 
                log = True, maxiter = 100)

#estima alfa para o valor de $\bar{c}$ desejado, pois se inicia o 
#looping no ramo superior
alfaG = secante(cImag, x0=a1, x1=a2, tol=1e-04, 
                args=(ReG, cG), log=True, maxiter=10)

#anexa os valores calculados nos respectivos vetores
alfaVetor.append(alfaG)
ReVetor.append(ReG)
cVetor.append( cG[1] )

def curvaEstabilidade(cE1, cE2, ReInicio, ymInicio, dRe, dAlfa, args=(), incrementaRe=True):
    '''
    Contrói a curva de estabilidade da equação de Orr-Sommerfeld para o intervalo
    de Re e c_i desejado.
    
    Parameters
    ----------
    cE1 : complex64
        Primeira estimativa inicial de c.
    cE2 : complex64
        Segunda estimativa inicial de c.
    ReInicio : int
        Valor de Re por onde será iniciado o calculo da curva. 
    ymInicio : float
        Ponto fora da camada limite por onde será iniciada a integração da
        equação de Orr-Sommerfeld. Recomenda-se utilizar 4,5. 
    dRe : int
        Incremento no numero de Reynolds. Se dRe = 500 e Re = 10.000 na primeira
        iteração, o próximo valor utilizado será Re = 9.500.
    dAlfa : float
        Incremento no valor de alfa entre as iterações. Semelhante ao exemplo
        dado para dRe. 
    *args : 
        Demais parâmetros necessários para resolver a função, por exemplo, 
        estimativas de alfa e quantidade de passos de integração.
    incrementaRe : bool
        Determina se a simulação iniciará pelo ramo inferior ou superior da 
        curva de estabilidade neutra. Se utilizar True, inicia-se pelo ramo superior.
        Do contrário, a construção da curva iniciará no ramo inferior.

    Returns
    -------
    alfaVetor : List.
        Retorna uma lista com os valores dos alfas necessários para 
        plotar a curva de estabilidade.
    cVetor : List of complex64.
        Retorna uma lista com os valores dos c que são solução da equação de Orr-Sommerfeld. Pode-se
        utilizar a parte real de c para construção da curva de estabilidade.
    ReVetor : List of int.
        Retorna uma lista com os valores de Re necessários para plotar a 
        curva de estabilidade.

    '''
    '''
    Faz o incremento de Reynolds no ramo superior da curva. 
    Se incrementaRe = False, a simulação ocorrerá no ramo inferior da curva. 
    Assim, alfa será incrementado.
    '''
    while (ReVetor[-1] <= 10000):
        
        if (incrementaRe):
            
            try:            
                #Incrementa e fixa $Re_{\delta_1}$
                ReG = ReVetor[-1] + dRe 
    
                #calculo das estimativas de $\bar{c}$
                cG[0] = extrapola(ReVetor, cVetor,ReG)
                cG[1] = 0.999 * cG[0]                      
            
                #calculo das estimativas de $\bar{\alpha}$
                a1 = extrapola(ReVetor,alfaVetor,ReG)    
                a2 = 0.999 * a1
                
                #busca o valor de $\bar{\alpha}$ para $\bar{c_i}$ desejado
                alfaG = secante(cImag, x0=a1, x1=a2, tol=1e-04, 
                                args=(ReG, cG), log=False, maxiter=10)
            
                #guarda os valores
                alfaVetor.append(alfaG)
                ReVetor.append(ReG)
                cVetor.append( cG[1] )
            
            except:
                '''
                #Se as estimativas oferecidas não são solução da equação de 
                Orr-Sommerfeld, diminui-se o intervalo de calculo por 4.
                '''            
                dRe /= 4   
        
            if (len(alfaVetor) > 1):            
                '''
                Calcula a diferença entre o alfa da solução atual com o 
                da anterior. Se a diferença for positiva, continua-se a 
                solução mostrada acima. Do contrário, o código passa a 
                buscar o valor de Re para o c desejado.
                '''            
                dif  = alfaVetor[-1] - alfaVetor[-2]
                if (dif < 0) and (abs(dif) < abs(dAlfa/10)):
                    incrementaRe = False  #fixa o $\bar{\alpha}$
                    dRe = dReMax           
        else:
            try: 
                #incrementa e fixa o valor de $\bar{\alpha}$
                alfaG = alfaVetor[-1] + dAlfa

                #estimavas para $\bar{c}$
                cG[0] = extrapola(alfaVetor,cVetor,alfaG)     
                cG[1] = 0.999 * cG[0]        
            
                #estimativas para $Re_{\delta_1}$
                Re1 = ReVetor[-1]   
                Re2 = 0.999 * Re1

                #busca o valor de $Re_{\delta_1}$ para o 
                #$\bar{c_i}$ desejado
                ReG = secante(cImag2, x0=Re1, x1=Re2, tol=1e-02, 
                              args=(alfaG, cG), log=True, maxiter=10)
           
                #guarda os valores
                alfaVetor.append(alfaG)
                ReVetor.append(ReG)
                cVetor.append( cG[1] )
            
            except ValueError as e:
                '''
             #Se as estimativas oferecidas não são solução da equação de 
            Orr-Sommerfeld, diminui-se o intervalo de calculo por 4.
                '''    
                dAlfa /= 4  

            if (len(ReVetor) > 1): 
                '''
                Verifica se a diferença entre Re da simulação atual com o da anterior.
                Se a diferença entre estes for positiva, continua-se com alfa fixo. 
                Do contrário, fixa-se Re através da primeira parte do looping.
                '''
                if (ReVetor[-1] - ReVetor[-2] > dRe) and (dRe >= 0.01):
                    incrementaRe = True   
        return alfaVetor, cVetor, ReVetor

#imprime os resultados
print ("alfa:", alfaVetor)
print ("c:", cVetor)
print ("Re:", ReVetor)

