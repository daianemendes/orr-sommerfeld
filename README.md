Este repositório contém dois módulos e um código em linguagem Python versão 3.6 para o cálculo da equação de Orr-Sommerfeld e obtenção da curva de estabilidade de acordo com o valor de c_i desejado. 

Esses códigos foram desenvolvidos para o meu Trabalho de Conclusão do Curso de Engenharia Aeroespacial da UFSC campus Joinville, que pode ser acessado através deste [link](https://repositorio.ufsc.br/handle/123456789/218726). 

Basicamente, escolhe-se um valor para o número de Reynolds, alfa e c_i. Este último é a parte imaginária da velocidade da onda de perturbação. 
Assim, o código faz o cálculo da equação de Orr-Sommerfeld através do método de Runge-Kutta de 4ª ordem com Gram-Schmidt. Para mais informações, leia a documentação.





